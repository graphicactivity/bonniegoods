// Git init
$(document).ready(function(){
    // Animate on scroll
    new WOW().init();
    // Header transition
    $(function() {
        // Cache object containing the header element
        var header = $(".noCrown");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 600) {
                header.removeClass('noCrown').addClass("showCrown");
            } else {
                header.removeClass("showCrown").addClass('noCrown');
            }
        });
    });
    // Show slide in/out left nav
    $('#menuBlock').click(function(){
        $(this).toggleClass('invert');
		$('.site-nav').toggleClass('open');
        $('.storeNav').toggleClass('invert');
	});
    // Hide slide in/out nav
    $('.container, container-fluid').mouseover(function(){
        $('.site-nav').removeClass('open');
        $('#menuBlock').removeClass('invert');
        $('.storeNav').removeClass('invert');
    });
    // Hide Announcement
    $('.closeAnnouncement').click(function(){
        $('.announcement').addClass('closeAnno');
    });
});
