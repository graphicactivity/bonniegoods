<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */


$customConfig = array(

    '*' => array(
        // 'devMode' => true,
        'omitScriptNameInUrls' => true,
        'env' => '',
        'extraAllowedFileExtensions' => 'json', # Required for embedded assets plugin
        'errorTemplatePrefix' => 'pages/error-pages/',
        //'useEmailAsUsername' => true,
        'templateCacheDisabled' => true,
        'postCpLoginRedirect' => 'entries'
    ),

    'production' => array(
        'devMode' => false,
        'siteUrl' => 'https://bonniegoods.co.nz',
        'env' => 'production',
        'templateCacheDisabled' => false
    ),

    'stage' => array(
        'siteUrl' => 'http://bonnie.graphicactivity.co.nz',
        'cooldownDuration' => 0,
        'env' => 'stage'
    ),
    'local' => array(
        'devMode' => true,
        'siteUrl' => 'http://bonniegoods.local',
        'cooldownDuration' => 0,
        'env' => 'local'
    ),

);

// If a local config file exists, merge any local config settings
if (is_array($customLocalConfig = @include(CRAFT_CONFIG_PATH . 'local/general.php')))
{
    $customGlobalConfig = array_merge($customConfig['*'], $customLocalConfig);
    $customConfig['*'] = $customGlobalConfig;
}

return $customConfig;
